# CWDC Whitepaper

CWDC - Community Web Digital Currency, comunity open source alternative to CBDC.

## Introduction

- Need simple, scalable, distributed community currency
- There must be a authority reperesenting trust behind, defined by community
- Blockchain is inherently slow and expensive with no authority behind
- Blockchain problems with fraud, theft and hacking and cannot be solved and repaired

## Idea

- Open source implementation of digital currency (DC) node and tools
- Community run digital currency node instance
- Community define transfer fees and manage reservs
- Based on cryptography, proposed [JWT](https://jwt.io/)
- Web server node with [Open API](https://www.openapis.org/) interface
- Client library for simple integrations
- Client Web app as default UI
- One node -- one community -- one community authority
- Scalability by running community nodes `community.currency.net`
- Node identified by `domain name` and `domain certificate`
- Account defined by `Public and Private Key Pair`
- Account local node address `account-id-or-alias`
- Account global address`account-id-or-alias@community.currency.net`
- Account data strictly managed and storred by comunity node
- Online global and local node transfers
- Offline community local node transfers, reason to force doble spend prevention

[CWDC Schema](https://viewer.diagrams.net/?tags=%7B%7D&highlight=0000ff&edit=_blank&layers=1&nav=1&title=CWDC.drawio#R7VtZc6M4EP41edwU5nDsRw7HIQvEB1nHfpnCIGNhDhdgc%2Fz6bXHExjiZTE12kq1iahihTy2p1d3qbkvMDSN66Tg09ls1sJB7Q1NWesNINzTd6w04KAiSlUi%2FXwF2iK2K6ATMcY4qkKrQA7ZQ1CCMg8CN8b4JmoHvIzNuYEYYBkmTbBO4zVn3ho1awNw03Da6wFa8rdfVH54aHhC2t9XUA%2FqubPCMmrhaSbQ1rCA5g5jRDSOGQRCXb14qIpcIr5ZL2e%2F%2BjdZXxkLkxx%2Fp8CL0%2BGgVi%2BNVHPXvFOZvfnr8i660cTTcQ7XiYLNxsY8qpuOslgSyQDBVNQjjbWAHvuGOTqgQBgffQmQ6CmonGiUI9gD2AHRQHGeVlo1DHAC0jT23aj2iMMYge97Ftg%2FYOojjwIMGy4i2xcCEquSMsPOmJCooCg6hid5bfr8yKSO0UfwOIdN71RiYOgo8FIcZdAyRa8T42OTEqGzOfqU7qQVeKs38ipb6LS09RygEpNdSE5jZnrxir7DsV5kqxhq5kyDCMQ4asr0UekyUJRTd%2BWhf7imiT6OubHBKdCFUM0iWERs3DF9W6fvoaN%2FQQgpKpcXJg0avMoFdL9KDmVPYeJhRphQcFcZirIxj1Iw7mp55VB0%2BUcVhbnkmlh9Wrulr%2BzXNDmVnROr71Yslrhkb6rytijz1VDyyvVpw26WXusricbukY9%2F0hr21Nx3KHudaGZ%2FIEm9rkk0ePMFLB41Hd7LI27KUJms65Vb08CCPV%2Fv1OBnKWBvPnB2rSjzMKRyNBUcRnlVnGqm5aav6DspRWTrPtuqclfkIKznrycx2%2B5TB%2BCI%2FgHV7xiKNJnPZme1WgpbLtOw0aIpnMtb2pqdFqznsDgnmy1UsjzUO%2BqVPc9k2HcCwfGzwXz6DJe1mSzp1gZ4l9BrQL5kdkRsnYyEDLJPxAPTxiDV2qUfljOLQv5TG8mUWNCSxkxltNAVOQJo7KlezJFVpNVF086BhKn0SqZ6ij1hFEhxVTFLFGUVQ9hQiDTFhljkfafOEUqRd9DRPODPfxZqYcIruSrDCSNXtA%2BDFuJrIpgqgijTK0eVKW7ypFGgk0yS55i2DcallRtElD1QK81PAR6zqPMxzT%2FhLFMcmfGWKo1alFqmY8G3Riv4P0MmxNib8zmhtAe366KCR9Up8MabiPJOS9IPxKAraY5AB9HUj8q5iQVadWf%2BKtMemvfLcaA26lXOT1SST6PWCqlqrY7w8OuvxM2iRWLvMghYdpIMF6DuwSrnYEReWdt2KHL5tRfNL2SbH1YXVmuNhhBbWcY1P1rvyVh7wATvGPVgPKuEL5DTNZWnEgd5SWSJ1NYOyrLdlAPtp%2FeD6sKdPsqCmubpbZjMM3I2H%2B7U%2Fy4m9gkXk2pzPVMyCRaicUuyxaaRJ6oHg2pxlTjhoJSflEsqpXe5VGUqZ1Ft70hzfU4Z4WpmmT%2BmZbgmwut1k%2FqiVe%2FlerUrQLIyvP5ISrOmxtCR9BNYqQx2sTdcCNX%2BUNGIlxKpLX1CV901rGCgMeMOcJX6aEVzijwXD3NlF2BQDNwgLD85sij%2BvgY74Z5S%2BH%2Braganu0K%2Byjyr9GlTV5JTLsIMS2p5lMTX2%2BZHsrhXJ3k00%2FMBHzRzhM6P%2F4KPR%2F%2B5Lo%2F%2FgrejPdNG%2Fi%2F5d9O%2Bifxf9u%2Bh%2FJfqzg28W%2FYe%2FFv3%2Fo2OGT0whmPpM6qcpBPuVKQRDdYL%2FGsH33srd2grocrcud%2Btyty5363K3LnejyU3St8rdGPpbpBARBPyYJxd7AJiuEUXYrOF77NZkIOQwe6mmKSpLUrnl6qqUnjdK2XltgkIMQkNhBX5m2sL%2BqSOnquskwMDj6RcB3fxFAEhziJKxqteFybyy8RtWxPzu%2Bd8HDeBLdDb80lSTbYlWXEgwVt8FpoU1JJ19m7yJgecdfBxn1%2B8Pdyg2t5Xln8n99QqW4MkWxyjaG4XcktAgeecGhH%2FmC%2BnBHTeiC82EwQ5d85J7YmWFGDgB%2FlK3FMVB6knd9hlS3HBALRYNvQKmL9DhVbQY4pJy%2BMbAd0VvaL8ySO8CI%2BewDVqORIM6a%2FdSm3wBcbs7rFHog%2FeIbrFJ8nVhH8rFCxizher4cZbPm2CRxNf8POE3qkS%2F1aP5C%2BBTok%2Bv6SeodvjhqD8ZftofKvyi4%2FhMj3D3UY8w%2BFKP0P5s4Ps724%2BKlqW%2BVLTte6yfO9srP%2Fk7Z9s5W5q7uKT9emf75o0j2zbh7tSqO7XqTq26U6vu1Ko7taL77Pe6cWTaN44zBKHsGLUCWR3AztKwOuJgD0LMvYvX8K%2Fp4v0PSH%2FhdYN9wzdJDJobG%2FSjRw9SeG73vn0tfH2CdC%2B%2F5vpoolBjny5etn2v%2BD8Wb3%2Fwx8QL1dOX%2F%2BXh2un%2FTzCjfwE%3D)

## Authors and acknowledgment

- Peter Rybar \
  [pr.rybar@gmail.com](mailto:pr.rybar@gmail.com) \
  [LinkedIn](https://www.linkedin.com/in/peter-rybar/)

## Idea validation

<https://www.protocol.com/fintech/molly-white-web3-crypto-skeptic>

I think it **would be really valuable for there to be more of a cash equivalent** to something like that where you actually get the same privacy and surveillance expectations of cash with a digital currency. It’s not being traced as closely as digital transactions are. You're allowed to make small transactions very privately.

I think that would be really beneficial for society to have something like that. But I think **as soon as you start looking at crypto and blockchains, you end up with a more speculative asset that has a lot of inherent flaws and tends to not work so well as currency**. I remain hopeful that there might be some sort of digital cash in our future but **I don't necessarily expect that it will look like a cryptocurrency**.

## Project status

Writing whitepaper and implementing prototype.
